<?php
/**
 * Created by IntelliJ IDEA.
 * User: anthonyfragapane
 * Date: 2/8/18
 * Time: 7:46 PM
 */

class Customer
{
    /**
     * @var int
     */
    private $customerId;
    /**
     * @var string
     */
    private $customerUserName;
    /**
     * @var string
     */
    private $customerPassword;
    /**
     * @var string
     */
    private $customerFirst;
    /**
     * @var string
     */
    private $customerLastName;
    /**
     * @var int
     */
    // private $customerPhone;
    // /**
    //  * @var string
    //  */
    // private $customerAddress;

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * Customer constructor.
     * @param int $customerId
     * @param float $customerUserName
     * @param float $customerEmail
     * @param int $customerPhone
     * @param string $customerFirstName
     * @param string $customerLastName
     */
    public function __construct(int $customerId, string $customerUserName, string $customerPassword, string $customerFirstName, string $customerLastName )
    {
        $this->customerId = $customerId;
        $this->customerPassword = $customerPassword;
        $this->customerUserName = $customerUserName;
        $this->customerFirstName = $customerFirstName;
        $this->customerLastName = $customerLastName;

    }


    public function getCustomerUserId(): int
    {
        return $this->customerId;
    }

    /**
     * @return string
     */
    public function getCustomerUserName(): string
    {
        return $this->customerUserName;
    }




    /**
     * @return string
     */
    public function getCustomerFirstName(): string
    {
        return $this->customerFirstName;
    }

    /**
     * @return string
     */
    public function getCustomerLastName(): string
    {
        return $this->customerLastName;
    }
    public function getCustomerAddress(): string
    {
        return $this->customerAddress;
    }

    public function getCustomerPassword(): string
    {
        return $this->customerPassword;
    }

}
/**
 * Customer constructor.
 * @param $customerId int
 * @param $customerUserName float
 * @param $customerEmail float
 * @param $customerPhone int
 * @param $customerFirstName string
 * @param $customerLastName string
 */
