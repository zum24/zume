<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexsolinger
 * Date: 2/8/18
 * Time: 7:51 PM
 */

class Payment
{
    /**
     * @var int
     */
    private $cardNumber;

    /**
     * @var int
     */
    private $civ;

    /**
     * @var string
     */
    private $cardHolderName;

    /**
     * @var int
     */
    private $zipCode;

    /**
     * Payment constructor.
     * @param int $cardNumber
     * @param int $civ
     * @param string $cardHolderName
     * @param int $zipCode
     */
    public function __construct($cardNumber, $civ, $cardHolderName, $zipCode)
    {
        $this->cardNumber = $cardNumber;
        $this->civ = $civ;
        $this->cardHolderName = $cardHolderName;
        $this->zipCode = $zipCode;
    }

    /**
     * @return int
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @return int
     */
    public function getCiv()
    {
        return $this->civ;
    }

    /**
     * @return string
     */
    public function getCardHolderName()
    {
        return $this->cardHolderName;
    }

    /**
     * @return int
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
}