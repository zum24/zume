<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 3/8/2018
 * Time: 5:41 PM
 */

require_once __DIR__ . "/../models/Vehicle.php";
require_once __DIR__ . "/../models/Customer.php";

class Ride
{
    /**
     * @var Customer
     */
    private $rideCustomer;
    /**
     * @var Vehicle
     */
    private $rideVehicle;

    /**
     * Ride constructor.
     * @param int $rideID
     * @param int $customerID
     * @param int $vehicleID
     */
    public function __construct(Customer $customer, Vehicle $vehicle)
    {
        $this->rideCustomer = $customer;
        $this->rideVehicle = $vehicle;
    }

    /**
     * @return Customer
     */
    public function getRideCustomer(): Customer
    {
        return $this->rideCustomer;
    }

    /**
     * @return Vehicle
     */
    public function getRideVehicle(): Vehicle
    {
        return $this->rideVehicle;
    }


}