<?php
/**
 * Created by IntelliJ IDEA.
 * User: Amalia
 * Date: 2/8/2018
 * Time: 7:53 PM
 */

class Admin
{
    /**
     * @var String
     */
    private $adminId;
    /**
     * @var String
     */
    private $adminPassword;

    /**
     * @return mixed
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * @return mixed
     */
    public function getAdminPassword()
    {
        return $this->adminPassword;
    }

}