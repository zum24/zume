<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 04/06/2018
 * Time: 6:08 PM
 */



$errors = [];
$ride = null;
$vehiclesAvailable = VehicleRepository::getVehiclesByStatus(Status::Available);
if (isset($_GET['requestRide'])) {
    //check forms for content
    if (!isset($_GET['customerID'])) $errors[] = 'customerID not set!';
    if (!isset($_GET['vehicleID'])) $errors[] = 'vehicleID not set!';
    $customerID = $_GET['customerID'];
    $vehicleID =  $_GET['vehicleID'];

    //server-side validation and variable assignments from repository calls
    if (!is_numeric($customerID)) $errors[] = 'customerID incorrect type';
    if (!is_numeric($vehicleID)) $errors[] = 'vehicleID incorrect type!';
    $customer = CustomerRepository::getCustomerbyCustomerId($customerID);
    $vehicle = VehicleRepository::getVehicleById($vehicleID);
    if (!in_array($vehicle, $vehiclesAvailable)) $errors[] = 'Vehicles is not available!';

    //si hay errors, no haci a ride
    if (!count($errors)) {
        //only load the libraries if we're going to use them!
        require_once __DIR__ . '/Customer.php';
        require_once __DIR__ . '/Vehicle.php';
        require_once __DIR__ . "/../repositories/VehicleRepository.php";
        require_once __DIR__ . '/Ride.php';
        $ride = new Ride($customer, $vehicle);
    }
}
//End Controller, Begin View
?>

<!DOCTYPE HTML>
<html>

<head>
    <title>Zum Map</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script>
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(30.229722, -97.753858),
            mapTypeId: 'roadmap'
        });

        var iconBase = 'http://afragapa.create.stedwards.edu/zum/';
        var icons = {
            zumCar: {
                icon: iconBase + 'zumCar.png'
            },
            zumer: {
                icon: iconBase + 'zumer.png'
            }
        };
        let vehiclePositions = [
            <?php
            $out = "";
            foreach($vehiclesAvailable as $availableVehicle)
            {
                $lat = $availableVehicle->getVehicleLat();
                $long = $availableVehicle->getVehicleLong();
                $type = "vehicle";
                $out .= "{position: new google.maps.LatLng($lat,$long), type: '$type'},";
            }
            $out = rtrim($out,',');
            ?>
        ];
        console.log(vehiclePositions);

        let features = [
            {
                position: new google.maps.LatLng(30.238718, -97.755153),
                type: 'zumCar'
            }, {
                position: new google.maps.LatLng(30.245432, -97.751520),
                type: 'zumCar'
            }, {
                position: new google.maps.LatLng(30.227467, -97.762964),
                type: 'zumCar'
            },{
                position: new google.maps.LatLng(30.229722, -97.753858),
                type: 'zumeCar'
            }

        ];

        // Create markers.
        features.forEach(function(feature) {
            var marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                map: map
            });
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtfiMWgpOC750Sm7ehjyNXDPQtgFRP7Eo&callback=initMap">
</script>

</body>
</html>

