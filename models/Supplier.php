<?php
/**
 * Created by IntelliJ IDEA.
 * User: anthonyfragapane
 * Date: 2/8/18
 * Time: 7:46 PM
 */

class Supplier
{
    /**
     * @var int
     */
    private $supplierId;
    /**
     * @var string
     */
    private $supplierPassword;
    /**
     * @var string
     */

    private $supplierUserName;
    /**
     * @var string
     */
    private $supplierFirstName;
    /**
     * @var string
     */
    private $supplierLastName;
    private $fleetId;
    
    public function getSupplierId(): int
    {
        return $this->supplierId;
    }

    /**
     * Customer constructor.H
     * @param int $supplierId
     * @param float $supplierUserName
     * @param float $customerEmail
     * @param int $customerPhone
     * @param string $supplierFirstName
     * @param string $supplierLastName
     *@param int $fleetId
     */
    public function __construct(int $supplierId, string $supplierUserName, string $supplierPassword, string $supplierFirstName, string $supplierLastName, $fleetId)
    {
        $this->supplierId = $supplierId;
        $this->supplierPassword = $supplierPassword;
        $this->supplierUserName = $supplierUserName;
        $this->supplierFirstName = $supplierFirstName;
        $this->supplierLastName = $supplierLastName;
        $this->fleetId = $fleetId;
    }


    public function getSupplierUserId(): int
    {
        return $this->supplierId;
    }

    /**
     * @return string
     */
    public function getSupplierUserName(): string
    {
        return $this->supplierUserName;
    }




    /**
     * @return string
     */
    public function getSupplierFirstName(): string
    {
        return $this->supplierFirstName;
    }

    /**
     * @return string
     */
    public function getSupplierLastName(): string
    {
        return $this->supplierLastName;
    }


    public function getSupplierPassword(): string
    {
        return $this->supplierPassword;
    }
    public function getSupplierFleetId(): string
    {
        return $this->fleetId;
    }

}
/**
 * Customer constructor.
 * @param $supplierId int
 * @param $supplierUserName float
 * @param $customerEmail float
 * @param $customerPhone int
 * @param $supplierFirstName string
 * @param $supplierLastName string
 */
?>