<?php
/**
 * Created by IntelliJ IDEA.
 * User: Amalia
 * Date: 4/4/2018
 * Time: 10:16 AM
 */
class User
{
    /**
     * @var int
     */
    private $userId;
    /**
     * @var string
     */
    private $userName;
    /**
     * @var string
     */
    private $userPassword;


    /**
     * Customer constructor.
     * @param int $userId
     * @param string $userName
     * @param string $userPassword

     */
    public function __construct(int $userId, string $userName, string $userPassword)
    {
        $this->userId = $userId;
        $this->userName = $userName;
        $this->userPassword = $userPassword;

    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return float
     */
    public function getUserPassword()
    {
        return $this->userPassword;
    }



}
?>