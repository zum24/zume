<?php
/**
 * Created by IntelliJ IDEA.
 * User: anthonyfragapane
 * Date: 2/8/18
 * Time: 7:56 PM
 */

class Vehicle
{
    /**
     * @var int
     */
    public $vehicleID;
    /**
     * @var string
     */
    public $fleetID;
    /**
     * @var string
     */
    public $vehicleVIN;
    /**
     * @var string
     */
    public $vehicleLPN;
    /**
     * @var boolean
     */
    public $vehicleStatus;
    /**
     * @var float
     */
    public $vehicleLat;
    /**
     * @var float
     */
    public $vehicleLong;

    /**
     * Vehicle constructor.
     * @param int $vehicleID
     * @param string $fleetID
     * @param string $vehicleVIN
     * @param string $vehicleLPN
     * @param boolean $vehicleStatus
     * @param float $vehicleLat
     * @param float $vehicleLong
     */
    public function __construct($vehicleID, $fleetID, $vehicleVIN, $vehicleLPN, $vehicleStatus, $vehicleLat, $vehicleLong)
    {
        $this->vehicleID = $vehicleID;
        $this->fleetID = $fleetID;
        $this->vehicleVIN = $vehicleVIN;
        $this->vehicleLPN = $vehicleLPN;
        $this->vehicleStatus = $vehicleStatus;
        $this->vehicleLat = $vehicleLat;
        $this->vehicleLong = $vehicleLong;
    }
    /**
     * @return int
     */
    public function getVehicleID()
    {
        return $this->vehicleID;
    }

    /**
     * @return string
     */
    public function getFleetID()
    {
        return $this->fleetID;
    }

    /**
     * @return string
     */
    public function getVehicleVIN()
    {
        return $this->vehicleVIN;
    }

    /**
     * @return string
     */
    public function getVehicleLPN()
    {
        return $this->vehicleLPN;
    }

    /**
     * @return Status
     */
    public function getVehicleStatus()
    {
        return $this->vehicleStatus;
    }

    /**
     * @param boolean $vehicleStatus
     */
    public function setVehicleStatus($vehicleStatus)
    {
        $this->vehicleStatus = $vehicleStatus;
    }


    /**
     * @return float
     */
    public function getVehicleLat(): float
    {
        return $this->vehicleLat;
    }

    /**
     * @return float
     */
    public function getVehicleLong(): float
    {
        return $this->vehicleLong;
    }




}