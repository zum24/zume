<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 3/1/2018
 * Time: 8:35 PM
 **/

class SupplyDB
{
    static $database;

//Create connection
    private static function dbConnect()
    {
        $servername = "localhost";
        $username = "jyoungcr_ZumeSA";
        $password = "ZumeSA";
        $dbName = "jyoungcr_ZumeVehicleSupplyDB";
        self::$database = new mysqli($servername, $username, $password, $dbName); // Check connection
        if (self::$database->connect_error) {
            die("Connection failed: " . self::$database->connect_error);
        }
        self::$database->query("USE " . $dbName);
    }

    public static function returnDB(): mysqli
    {
        self::dbConnect();
        return self::$database;
    }

    public static function scrubQuery($query): string
    {
        return self::returnDB()->real_escape_string($query);
    }

    public static function runSingleQuery(string $query)
    {
        $result = self::returnDB()->query($query);
        if (self::returnDB()->error)
        {
            echo self::returnDB()->error;
            return false;
        }
        else if ($result) {
            $assoc = $result->fetch_assoc();
        }
        return $assoc;
    }

    public static function runAllQuery(string $query)
    {
        $result = self::returnDB()->query($query);
        if (self::returnDB()->error)
        {
            echo self::returnDB()->error;
            return false;
        }
        else if ($result) {
            $data = array();
            while ($row = $result->fetch_assoc()) {
                array_push($data, $row);
            }
            return $data;
        }
        return [];
    }

    public static function getLastKey()
    {
        return self::returnDB()->insert_id;
    }

}