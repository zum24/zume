<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexsolinger
 * Date: 4/12/18
 * Time: 4:57 PM
 */

http_response_code(403);
return;

use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{

    protected $coffee = null;

    public function test__construct()
    {
        self::assertInstanceOf(Customer::class, $this->customer);
    }

    public function testGetCustomerId()
    {
        self::assertAttributeEquals(0, 'customerId', $this->customer);
    }

    public function testGetCustomerUserName()
    {
        self::assertAttributeEquals(' ', 'customerUserName', $this->customer);
    }

    public function testGetCustomerEmail()
    {
        self::assertAttributeEquals(' ', 'customerEmail', $this->customer);
    }

    public function testGetCustomerPhone()
    {
        self::assertAttributeEquals(5555555555, 'customerPhone', $this->customer);
    }

    public function testGetCustomerFirstName()
    {
        self::assertAttributeEquals(' ', 'customerFirstName', $this->customer);
    }

    public function testGetCustomerLastName()
    {
        self::assertAttributeEquals(' ', 'customerLastName', $this->customer);
    }

    protected function setUp()
    {
        require_once __DIR__ . '/../models/Customer.php';
        $this->customer = new Customer(0, ' ', ' ', 5555555555, ' ', ' ');
    }
}