<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexsolinger
 * Date: 4/12/18
 * Time: 5:43 PM
 */

use PHPUnit\Framework\TestCase;

class VehicleTest extends TestCase
{

    protected $vehicle = null;

    public function test__construct()
    {
        self::assertInstanceOf(Vehicle::class, $this->vehicle);
    }

    public function testGetVehicleID()
    {
        self::assertAttributeEquals(' ', 'vehicleID', $this->vehicle);
    }

    public function testGetFleetID()
    {
        self::assertAttributeEquals(' ', 'fleetID', $this->vehicle);
    }

    public function testGetVehicleVIN()
    {
        self::assertAttributeEquals(' ', 'vehicleVIN', $this->vehicle);
    }

    public function testGetVehicleLPN()
    {
        self::assertAttributeEquals(' ', 'vehicleLPN', $this->vehicle);
    }

    public function testGetVehicleStatus()
    {
        self::assertAttributeEquals(0, 'vehicleStatus', $this->vehicle);
    }

    public function testGetVehicleLat()
    {
        self::assertAttributeEquals(000.000, 'vehicleLat', $this->vehicle);
    }

    public function testSetVehicleLat()
    {
        self::assertAttributeEquals(000.000, 'vehicleLat', $this->vehicle);
    }

    public function testGetVehicleLong()
    {
        self::assertAttributeEquals(000.000, 'vehicleLong', $this->vehicle);
    }

    public function testSetVehicleLong()
    {
        self::assertAttributeEquals(000.000, 'vehicleLong', $this->vehicle);
    }


    protected function setUp()
    {
        require_once __DIR__ . '/../models/Vehicle.php';
        $this->vehicle = new Vehicle(' ', ' ', ' ', ' ', 0, 0.00, 0.00);
    }
}
