<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 3/7/2018
 * Time: 1:10 PM
 */
require_once __DIR__ . "/lib/Response.php";
require_once __DIR__ . "/models/Vehicle.php";
require_once __DIR__ . "/repositories/VehicleRepository.php";

$response = new Response();
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);

switch ($method) {
    case 'GET':
        if (isset($_GET['vehicleID'])) {
            $vehicleID = filter_var($_GET['vehicleID'], FILTER_SANITIZE_STRING);
            $vehicle = VehicleRepository::getVehicleById($vehicleID);
            $response->pushData($vehicle);
            http_response_code(200);
            $response->echoJSONString();
        }
        if (isset($_GET['fleetID'])) {
            $fleetID = filter_var($_GET['fleetID'], FILTER_SANITIZE_STRING);
            $vehicle = VehicleRepository::getVehiclesByFleetID($fleetID);
            $response->pushData($vehicle);
            http_response_code(200);
            $response->echoJSONString();
        }
        if (isset($_GET['vehicleStatus'])) {
            $vehicleStatus = filter_var($_GET['vehicleStatus'], FILTER_SANITIZE_STRING);
            foreach (VehicleRepository::getAllVehicles() as $vehicle) {
                $response->pushData($vehicle);
            }
            http_response_code(200);
            $response->echoJSONString();
        }
        else {
            foreach (VehicleRepository::getAllVehicles() as $vehicle) {
                $response->pushData($vehicle);
            }
            http_response_code(200);
            $response->echoJSONString();
        }
        break;
    case 'PUT':
        $data = json_decode(file_get_contents("php://input"), true);
        $data = filter_var_array($data, FILTER_SANITIZE_STRING);
        if (!isset($data['vehicleID'])) {
            $response->pushError("(vehicleID) was not set for $method!");
        }
        if (!isset($data['fleetID'])) {
            $response->pushError("(fleetID) was not set for $method!");
        }
        if (!isset($data['vehicleVIN'])) {
            $response->pushError("(vehicleVIN) was not set for $method!");
        }
        if (!isset($data['vehicleLPN'])) {
            $response->pushError("(vehicleLPN) was not set for $method!");
        }
        if (!isset($data['vehicleStatus'])) {
            $response->pushError("(vehicleStatus) was not set for $method!");
        }
        if (!isset($data['vehicleLat'])) {
            $response->pushError("(vehicleLat) was not set for $method!");
        }
        if (!isset($data['vehicleLong'])) {
            $response->pushError("(vehicleLong) was not set for $method!");
        }
        if (!$response->getErrorCount()) {
            $vehicle = new Vehicle($data['vehicleID'], $data['fleetID'], $data['vehicleVIN'], $data['vehicleLPN'],
                $data['vehicleStatus'], $data['vehicleLat'], $data['vehicleLong']);
            $querySuccess = VehicleRepository::updateVehicle($vehicle);
            if ($querySuccess) {
                http_response_code(200);
            } else {
                http_response_code(400);
            }
        } else {
            http_response_code(400);
        }
        $response->echoJSONString();
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $data = filter_var_array($data, FILTER_SANITIZE_STRING);
        if (!isset($data['vehicleID'])) {
            $response->pushError("(vehicleID) was not set for $method!");
        }
        if (!isset($data['fleetID'])) {
            $response->pushError("(fleetID) was not set for $method!");
        }
        if (!isset($data['vehicleVIN'])) {
            $response->pushError(" (vehicleVIN) was not set for $method!");
        }
        if (!isset($data['vehicleLPN'])) {
            $response->pushError("(vehicleLPN) was not set for $method!");
        }
        if (!isset($data['vehicleStatus'])) {
            $response->pushError("(vehicleStatus) was not set for $method!");
        }
        if (!isset($data['vehicleLat'])) {
            $response->pushError("(vehicleLat) was not set for $method!");
        }
        if (!isset($data['vehicleLong'])) {
            $response->pushError("(vehicleLong) was not set for $method!");
        }
        if (!$response->getErrorCount()) {
            //vehicleID = -1
            $vehicle = new Vehicle(-1, $data['fleetID'], $data['vehicleVIN'], $data['vehicleLPN'],
                $data['vehicleStatus'], $data['vehicleLat'], $data['vehicleLong']);
            $querySuccess = VehicleRepository::insertVehicle($vehicle);
            if ($querySuccess) {
                http_response_code(200);
                $response->pushData(['vehicleID' => SupplyDB::getLastKey()]);
            } else {
                http_response_code(400);
            }
        } else {
            http_response_code(400);
        }
        $response->echoJSONString();
        break;
    case 'DELETE':
        http_response_code(405);
        $response->echoJSONString();
        break;
    case 'OPTIONS':
        header('Allow: OPTIONS, GET, POST, PUT, DELETE');
        break;
    default:
        http_response_code(204);
        $response->echoJSONString();
}