<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Young
 * Date: 3/07/2018
 * Time: 12:30 pm
 */
//load our SupplyDB driver and our vehicle model whenever VehicleRepository is loaded
require_once __DIR__ . '/../lib/SupplyDB.php';
require_once __DIR__ . '/../models/Vehicle.php';

class VehicleRepository
{

    public static function getVehicleById(int $vehicleID): Vehicle
    {
        //database security
        $vehicleID = SupplyDB::scrubQuery($vehicleID);
        //get the raw data from the SupplyDB
        $rawVehicleDatum = SupplyDB::runSingleQuery("SELECT * FROM Vehicle WHERE vehicleID ='$vehicleID'");

        if ($rawVehicleDatum) {
            return new Vehicle($rawVehicleDatum['vehicleID'], $rawVehicleDatum['fleetID'], $rawVehicleDatum['vehicleVIN'],
                $rawVehicleDatum['vehicleLPN'], $rawVehicleDatum['vehicleStatus'], $rawVehicleDatum['vehicleLat'], $rawVehicleDatum['vehicleLong']);
        }
        //return null if cant find vehicle with that ID
        return null;
    }

    //this is the same as above, except it gets *all* the vehicles with certain fleetID
    public static function getVehiclesByFleetID(string $fleetID): array
    {
        $fleetID = SupplyDB::scrubQuery($fleetID);
        $rawVehicleData = SupplyDB::runAllQuery("SELECT * FROM Vehicle WHERE fleetID='$fleetID'");
        if ($rawVehicleData) {
            $output = [];
            foreach ($rawVehicleData as $rawVehicleDatum) {
                $output[] = new Vehicle($rawVehicleDatum['vehicleID'], $rawVehicleDatum['fleetID'], $rawVehicleDatum['vehicleVIN'],
                    $rawVehicleDatum['vehicleLPN'], $rawVehicleDatum['vehicleStatus'], $rawVehicleDatum['vehicleLat'], $rawVehicleDatum['vehicleLong']);
            }
            return $output;
        }
        return [];
    }

    public static function getVehiclesByStatus(int $vehicleStatus): array
    {
        $vehicleStatus = SupplyDB::scrubQuery($vehicleStatus);
        $rawVehicleData = SupplyDB::runAllQuery("SELECT * FROM Vehicle WHERE vehicleStatus='$vehicleStatus'");
        if ($rawVehicleData) {
            $output = [];
            foreach ($rawVehicleData as $rawVehicleDatum) {
                $output[] = new Vehicle($rawVehicleDatum['vehicleID'], $rawVehicleDatum['fleetID'], $rawVehicleDatum['vehicleVIN'],
                    $rawVehicleDatum['vehicleLPN'], $rawVehicleDatum['vehicleStatus'], $rawVehicleDatum['vehicleLat'], $rawVehicleDatum['vehicleLong']);
            }
            return $output;
        }
        return [];

    }

    public static function getAllVehicles(): array
    {
        $rawVehicleData = SupplyDB::runAllQuery("SELECT * FROM Vehicle");
        if ($rawVehicleData) {
            $output = [];
            foreach ($rawVehicleData as $rawVehicleDatum) {
                $output[] = new Vehicle($rawVehicleDatum['vehicleID'], $rawVehicleDatum['fleetID'], $rawVehicleDatum['vehicleVIN'],
                    $rawVehicleDatum['vehicleLPN'], $rawVehicleDatum['vehicleStatus'], $rawVehicleDatum['vehicleLat'], $rawVehicleDatum['vehicleLong']);
            }
            return $output;
        }
        return [];
    }

    public static function insertVehicle(Vehicle $vehicle): bool
    {
        $vehicleID = $vehicle->getVehicleID();
        $fleetID = $vehicle->getFleetID();
        $vehicleVIN = $vehicle->getVehicleVIN();
        $vehicleLPN = $vehicle->getVehicleLPN();
        $vehicleStatus = $vehicle->getVehicleStatus();
        $vehicleLat = $vehicle->getVehicleLat();
        $vehicleLong = $vehicle->getVehicleLong();

        return SupplyDB::runSingleQuery("INSERT INTO Vehicle(vehicleID, fleetID, vehicleVIN, vehicleLPN, vehicleStatus, vehicleLat, vehicleLong) 
                                          VALUES ('$vehicleID', '$fleetID', '$vehicleVIN', '$vehicleLPN', '$vehicleStatus', '$vehicleLat', '$vehicleLong')");

    }

    public static function updateVehicle(Vehicle $vehicle): bool
    {
        $vehicleID = $vehicle->getVehicleID();
        $fleetID = $vehicle->getFleetID();
        $vehicleVIN = $vehicle->getVehicleVIN();
        $vehicleLPN = $vehicle->getVehicleLPN();
        $vehicleStatus = $vehicle->getVehicleStatus();
        $vehicleLat = $vehicle->getVehicleLat();
        $vehicleLong = $vehicle->getVehicleLong();

        return SupplyDB::runSingleQuery("UPDATE Vehicle SET vehicleID = '$vehicleID', fleetID = '$fleetID', 
                                                      vehicleVIN = '$vehicleVIN', vehicleLPN = '$vehicleLPN',
                                                      vehicleStatus = '$vehicleStatus',
                                                      vehicleLat = '$vehicleLat', vehicleLong = '$vehicleLong'
                                                      WHERE vehicleID = '$vehicleID'");
    }
}