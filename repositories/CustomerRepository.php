<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexsolinger
 * Date: 2/21/18
 * Time: 7:12 PM
 */

//load our database driver and our coffee model whenever CoffeeRepository is loaded
require_once __DIR__ .('../../Database.php');
require_once('/home/aalcanta/public_html/zum/models/Customer.php');

class CustomerRepository
{
    /**
     * @param int $customerId
     * @return Customer
     */
    public static function getCustomerbyCustomerUserName(string $customerUserName)
    {

        // Mitigate SQL injection attacks???
        $customerUserName = Database::scrubQueryParam($customerUserName);

        // get the raw data from the database
        $rawCustomerDatum = Database::query("SELECT * FROM customer WHERE customerUserName = '$customerUserName'");

        if ($rawCustomerDatum)
        {
            return new Customer($rawCustomerDatum['customerId'], $rawCustomerDatum['customerUserName'],
                $rawCustomerDatum['customerPassword'],
                $rawCustomerDatum['customerFirstName'], $rawCustomerDatum['customerLastName']);
        }

        // if we couldn't find a Customer with the given id, return null
        return null;

    }

    // this gets *all* customers associated with an email
    public static function getCustomerByCustomerLastName(string $customerLastName) : array
    {
        $customerLastName = Database::scrubQueryParam($customerLastName);
        $rawCustomerData = Database::runQueryAll("SELECT * FROM Customer WHERE customerLastName='$customerLastName'");
        if ($rawCustomerData)
        {
            $output = [];
            foreach ($rawCustomerData as $rawCustomerDatum)
            {
                $output[] = new Customer($rawCustomerDatum['customerId'], $rawCustomerDatum['customerUserName'],
                    $rawCustomerDatum['customerPassword'],
                    $rawCustomerDatum['customerFirstName'], $rawCustomerDatum['customerLastName']);
            }
            return $output;
        }
        return [];
    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public static function insertCustomer(Customer $customer)
    {
        $customerId = NULL;
        $customerUserName = Database::scrubQueryParam($customer->getCustomerUserName());
        $customerPassword = Database::scrubQueryParam($customer->getCustomerPassword());
        $customerFirstName = Database::scrubQueryParam($customer->getCustomerFirstName());
        $customerLastName = Database::scrubQueryParam($customer->getCustomerLastName());


        return Database::query("INSERT INTO customer(customerId, customerUserName, customerPassword, customerFirstName, customerLastName,customerPhone, customerAddress) VALUES (DEFAULT, '$customerUserName', '$customerPassword', '$customerFirstName', '$customerLastName'");

    }

}
?>