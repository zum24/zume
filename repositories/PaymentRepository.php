<?php
/**
 * Created by IntelliJ IDEA.
 * User: anthonyfragapane
 * Date: 2/22/18
 * Time: 5:31 PM
 */

namespace PaymentRepo;


class PaymentRepository
{
    public static function getPaymentById (int $cardNumber): \Payment
    {
        //We want to make sure to mitigate SQL injection attacks!
        $cardNumber = Database::scrubQuery($cardNumber);
        //get the raw data from the database
        $rawPaymentDatum = Database::runQuerySingle("SELECT * FROM Payment WHERE cardNumber='$cardNumber'");
        //our Database class will give us a one dimensional array with the result of our query, or an empty array
        //the keys of the array will be txhe selected columns, in our case all columns
        //now let's make sure we actually got something back
        if ($rawPaymentDatum) {
            return new Payment($rawPaymentDatum['cardNumber'], $rawPaymentDatum['cardCSV'], $rawPaymentDatum['cardHolderName'], $rawPaymentDatum['zipCode'], $rawPaymentDatum['PaymentFirstName'], $rawPaymentDatum['PaymentLastName']);
        }
        //if we couldn't find a Payment with the given id, return null
        return null;
    }

    //this is the same as above, except it gets *all* the Payments associated with
    public static function getPaymentByEmail(string $cardHolderName): array
    {
        $cardHolderName = Database::scrubQuery($cardHolderName);
        $rawPaymentData = Database::runQueryAll("SELECT * FROM Payment WHERE cardHolderName='$cardHolderName'");
        if ($rawPaymentData) {
            $output = [];
            foreach ($rawPaymentData as $rawPaymentDatum) {
                return new Payment($rawPaymentDatum['cardNumber'], $rawPaymentDatum['cardCSV'], $rawPaymentDatum['cardHolderName'], $rawPaymentDatum['zipCode'], $rawPaymentDatum['PaymentFirstName'], $rawPaymentDatum['PaymentLastName']);
            }
            return $output;
        }
        return [];
    }

    public static function getAllPayments(): array
    {
        $rawPaymentDatum = Database::runQueryAll("SELECT * FROM Payment");
        if ($rawPaymentDatum) {
            $output = [];
            foreach ($rawPaymentDatum as $rawPaymentDatum) {
                $output[] =  new Payment($rawPaymentDatum['cardNumber'], $rawPaymentDatum['cardCSV'], $rawPaymentDatum['cardHolderName'], $rawPaymentDatum['zipCode'], $rawPaymentDatum['PaymentFirstName'], $rawPaymentDatum['PaymentLastName']);
            }
            return $output;
        }
        return [];
    }

    public static function insertPayment(Payment $Payment) : bool
    {
        $cardNumber = $Payment->getcardNumber();
        $cardCSV = $Payment->getcardCSV();
        $cardHolderName = $Payment->getcardHolderName();
        $zipCode = $Payment->getzipCode();


        return Database::runQuerySingle("INSERT INTO Payment(cardCSV, cardHolderName, zipCode ) VALUES ('$cardCSV', '$cardHolderName', '$zipCode')");

    }

    public static function updatePayment(Payment $Payment) : bool
    {
        $cardNumber = $Payment->getcardNumber();
        $cardCSV = $Payment->getcardCSV();
        $cardHolderName = $Payment->getcardHolderName();
        $zipCode = $Payment->getzipCode();


        return Database::runQuerySingle("UPDATE Payment SET cardCSV = '$cardCSV', cardHolderName = '$cardHolderName', zipCode = '$zipCode' WHERE cardNumber = '$cardNumber'");

    }

}