<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexsolinger
 * Date: 2/21/18
 * Time: 7:12 PM
 */

//load our database driver and our coffee model whenever CoffeeRepository is loaded
require_once __DIR__ .('../../Database.php');
require_once('/home/aalcanta/public_html/zum/models/Supplier.php');

class SupplierRepository
{
    /**
     * @param int $supplierId
     * @return Supplier
     */
    public static function getSupplierByUserName(string $supplierUserName)
    {

        // Mitigate SQL injection attacks???
        $supplierUserName = Database::scrubQueryParam($supplierUserName);

        // get the raw data from the database
        $rawSupplierDatum = Database::query("SELECT * FROM supplier WHERE supplierUserName = '$supplierUserName'");

        if ($rawSupplierDatum)
        {
            return new Supplier($rawSupplierDatum['supplierId'], $rawSupplierDatum['supplierUserName'],
                $rawSupplierDatum['supplierPassword'],
                $rawSupplierDatum['supplierFirstName'], $rawSupplierDatum['supplierLastName'],$rawSupplierDatum['fleetId']);
        }

        // if we couldn't find a Customer with the given id, return null
        return null;

    }

    public static function getSupplierByLastName(string $supplierLastName)
    {

        // Mitigate SQL injection attacks???
        $supplierLastName = Database::scrubQueryParam($supplierLastName);

        // get the raw data from the database
        $rawSupplierDatum = Database::query("SELECT * FROM supplier WHERE supplierLastName = '$supplierLastName'");

        if ($rawSupplierDatum)
        {
            return new Supplier($rawSupplierDatum['supplierId'], $rawSupplierDatum['supplierUserName'],
                $rawSupplierDatum['supplierPassword'],
                $rawSupplierDatum['supplierFirstName'], $rawSupplierDatum['supplierLastName'],$rawSupplierDatum['fleetId']);
        }

        // if we couldn't find a Customer with the given id, return null
        return null;

    }

    /**
     * @param Customer $customer
     * @return bool
     */
    public static function insertSupplier(Supplier $supplier)
    {
        $supplierId = NULL;
        $supplierUserName = Database::scrubQueryParam($supplier->getsupplierUserName());
        $supplierPassword = Database::scrubQueryParam($supplier->getSupplierPassword());
        $supplierFirstName = Database::scrubQueryParam($supplier->getSupplierFirstName());
        $supplierLastName = Database::scrubQueryParam($supplier->getSupplierLastName());
        $fleetId = Database::scrubQueryParam($supplier->getSupplierFleetId());


        return Database::query("INSERT INTO supplier(supplierId, supplierUserName,supplierPassword, supplierFirstName, supplierLastName, fleetId) VALUES (DEFAULT, '$supplierUserName', '$supplierPassword', '$supplierFirstName', '$supplierLastName','$fleetId')");

    }

}
