<?php
/**
 * Created by IntelliJ IDEA.
 * User: Amalia
 * Date: 4/4/2018
 * Time: 7:16 PM
 */

// Include config file

require_once('../repositories/SupplierRepository.php');
require_once('/home/aalcanta/public_html/zum/models/Supplier.php');


// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
$userFirstName = $userLastName = $userNumber = "";
$userFirstName_err = $userLastName_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{

    // Validate username
    if(empty(trim($_POST['username'])))
    {
        $username_err = "Please enter a username.";
    }
    else
    {
        $param_username = trim($_POST['username']);


        //this line tries to pull user with the username provided in the form. If a user with that specific username does not exist, null will be returned
        $stmt = SupplierRepository::getSupplierByUserName($param_username);

        //we want $stmt to be null to add user
        if($stmt==null)
        {
            $username = trim($_POST["username"]);

        }
        else
        {
            $username_err = "This username is already taken.";
        }


    }
    // Validate user first name
    if(empty(trim($_POST['userFirstName'])))
    {
        $userFirstName_err = "Please enter a first name.";
    }
    else
    {
        
        $userFirstName = trim($_POST["userFirstName"]);
    }

    // Validate user first name
    if(empty(trim($_POST['userLastName'])))
    {
        $userLastName_err = "Please enter a last name.";
    }
    else
    {
        $param_userLastName = trim($_POST['userLastName']);
        $userLastName = trim($_POST["userLastName"]);


    }

    // Validate password
    if(empty(trim($_POST['password'])))
    {
        $password_err = "Please enter a password.";
    }
    //need to add regex expression for specifications of password
    elseif(strlen(trim($_POST['password'])) < 6)
    {
        $password_err = "Password must have at least 6 characters.";
    }
    else
    {
        $password = trim($_POST['password']);
    }

    // Validate confirm password
    if(empty(trim($_POST["confirm_password"])))
    {
        $confirm_password_err = 'Please confirm password.';
    }
    else
    {
        $confirm_password = trim($_POST['confirm_password']);
        if($password != $confirm_password)
        {
            $confirm_password_err = 'Password did not match.';
        }
    }

    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)&& empty($userFirstName_err) && empty($userLastName_err))
    {
        // Set parameters
        $param_username = $username;
        
        $param_userFirstName = $userFirstName;
        $param_userLastName = $userLastName;
        $param_password = hash("sha256", $password); // Creates a password hash
        $fleetId = 'Phantom Fleet Services LLC';
        //$param_password = $password;
        $supplier = new Supplier(1, $param_username, $param_password , $param_userFirstName,  $param_userLastName, $fleetId);



        //user repo 'insertUserMethod' will return false if user was not successfully inserted in database.
        if(!SupplierRepository::insertSupplier($supplier) )
        {


            // Redirect to login page


            header("location: sLogin.php");
        }
        else
        {
            echo "Something went wrong. Please try again later.";
        }
    }





}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zum: The Future of RideSharing</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../bootstrap-landing/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <!-- Plugin CSS -->
    <link href="../bootstrap-landing/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-landing/css/creative.min.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery/jquery.min.js"></script>
    <script src="../bootstrap-landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../bootstrap-landing/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../bootstrap-landing/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/">Zum: The Future of Ridesharing</a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerRegister.php">Rider Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerLogin.php">Rider Log Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sRegister.php">Supplier Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sLogin.php">Supplier Login</a>
                </li>
            </ul>
        </div>

    </div>
</nav>


<header class="masthead text-center text-white d-flex" style = "height: 900px;">
    <div class="container my-auto">
        <div class="col-lg-10 mx-auto">
            <div class="wrap" style="margin-left:30px; marigin-top:50px;">

                <div class="wrapper" style = "padding-top: 5%;">
                    <h2>Sign Up</h2>
                    <p>Please fill this form to create an account.</p>

                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style =  "text-align: left; width: 500px;margin-left: auto;
    margin-right: auto;">

                        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>" >

                            <input type="text" name="username" placeholder = "Username" class="form-control" value="<?php echo $username; ?>">
                            <span class="help-block" style="color:red;"><?php echo $username_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($userFirstName_err)) ? 'has-error' : ''; ?>">

                            <input type="text" name="userFirstName"placeholder = "First Name"class="form-control" value="<?php echo $userFirstName; ?>">
                            <span class="help-block" style="color:red;"><?php echo $userFirstName_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($userLastName_err)) ? 'has-error' : ''; ?>">

                            <input type="text" name="userLastName"placeholder = "Last Name"class="form-control" value="<?php echo $userLastName; ?>">
                            <span class="help-block"style="color:red;"><?php echo $userLastName_err; ?></span>
                        </div>


                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">

                            <input type="password" name="password" placeholder = "Password"class="form-control" value="<?php echo $password; ?>">
                            <span class="help-block"style="color:red;"><?php echo $password_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">

                            <input type="password" name="confirm_password"placeholder = "Confirm Password" class="form-control" value="<?php echo $confirm_password; ?>">
                            <span class="help-block"style="color:red;"><?php echo $confirm_password_err; ?></span>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Submit">
                            <input type="reset" class="btn btn-default" value="Reset">
                        </div>
                        <p>Already have an account? <a href="sLogin.php">Log in here</a>.</p>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</header>





</body>
</html>





}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zum: The Future of RideSharing</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../bootstrap-landing/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <!-- Plugin CSS -->
    <link href="../bootstrap-landing/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-landing/css/creative.min.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery/jquery.min.js"></script>
    <script src="../bootstrap-landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../bootstrap-landing/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../bootstrap-landing/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/">Zum: The Future of Ridesharing</a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerRegister.php">Rider Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerLogin.php">Rider Log Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sRegister.php">Supplier Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sLogin.php">Supplier Login</a>
                </li>
            </ul>
        </div>

    </div>
</nav>


<header class="masthead text-center text-white d-flex" style = "height: 900px;">
    <div class="container my-auto">
        <div class="col-lg-10 mx-auto">
            <div class="wrap" style="margin-left:30px; marigin-top:50px;">

                <div class="wrapper" style = "padding-top: 5%;">
                    <h2>Sign Up</h2>
                    <p>Please fill this form to create an account.</p>

                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style =  "text-align: left; width: 500px;margin-left: auto;
    margin-right: auto;">

                        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>" >

                            <input type="text" name="username" placeholder = "Username" class="form-control" value="<?php echo $username; ?>">
                            <span class="help-block" style="color:red;"><?php echo $username_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($userName_err)) ? 'has-error' : ''; ?>">

                            <input type="text" name="userName"placeholder = "First Name"class="form-control" value="<?php echo $userFirstName; ?>">
                            <span class="help-block" style="color:red;"><?php echo $userFirstName_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($userLastName_err)) ? 'has-error' : ''; ?>">

                            <input type="text" name="userLastName"placeholder = "Last Name"class="form-control" value="<?php echo $userLastName; ?>">
                            <span class="help-block"style="color:red;"><?php echo $userLastName_err; ?></span>
                        </div>


                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">

                            <input type="password" name="password" placeholder = "Password"class="form-control" value="<?php echo $password; ?>">
                            <span class="help-block"style="color:red;"><?php echo $password_err; ?></span>
                        </div>

                        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">

                            <input type="password" name="confirm_password"placeholder = "Confirm Password" class="form-control" value="<?php echo $confirm_password; ?>">
                            <span class="help-block"style="color:red;"><?php echo $confirm_password_err; ?></span>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Submit">
                            <input type="reset" class="btn btn-default" value="Reset">
                        </div>
                        <p>Already have an account? <a href="sLogin.php">Log in here</a>.</p>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
</header>





</body>
</html>