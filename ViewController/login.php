<?php

require_once('../repositories/UserRepository.php');
require_once('/home/aalcanta/public_html/Zum/models/User.php');


// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
session_start();
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{

    // Check if username is empty
    if(empty(trim($_POST["username"])))
    {
        $username_err = 'Please enter username.';
    }
    else
    {
        $username = trim($_POST["username"]);
    }

    // Check if password is empty
    if(empty(trim($_POST['password'])))
    {
        $password_err = 'Please enter your password.';
    }
    else
    {
        $password = trim($_POST['password']);
    }

    // Validate credentials
    if(empty($username_err) && empty($password_err))
    {
        if($user = UserRepository::getUserByUserName($username))
        {
            // Prepare a select statement

            if($user!=null)
            {
                $user_password = $user->getUserPassword();
                //when hash the password use the following code
                //if(password_verify($username, $hashPassword);
                if($user_password == $password)
                {
                    session_start();
                    $_SESSION['username'] = $username;
                    $_SESSION['loggedIn'] = 1;
                    header("location: zumOrder.php");
                }
                else
                {
                    $password_err = 'The password you entered was not valid.';
                }
            }
            else
            {
                $username_err = 'No account found with that username.';
            }
        }
        else
        {
            echo "Oops! Something went wrong. Please try again later.";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zum: The Future of RideSharing</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="../bootstrap-landing/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="../bootstrap-landing/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-landing/css/creative.min.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery/jquery.min.js"></script>
    <script src="../bootstrap-landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../bootstrap-landing/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../bootstrap-landing/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="../bootstrap-landing/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/Zum/ViewController/">Zum: The Future of Ridesharing</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/Zum/ViewController/register.php">Rider Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/Zum/ViewController/login.php">Rider Log Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/Zum/ViewController/sRegister.php">Supplier Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/Zum/ViewController/sLogin.php">Supplier Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div class="wrap" style="margin-left:30px; marigin-top:50px;">
                    <h2>Rider Login</h2>
                    <p>Please fill in your credentials to login.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                            <label>Username</label>
                            <input type="text" name="username"class="form-control" value="<?php echo $username; ?>">
                            <span class="help-block"><?php echo $username_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control">
                            <span class="help-block"><?php echo $password_err; ?></span>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Login">
                        </div>
                        <p>Don't have an account? <a href="customerRegister.php">Sign up now</a>.</p>
                    </form>
                </div>
                <hr>
            </div>

        </div>
    </div>
</header>


</body>
</html>