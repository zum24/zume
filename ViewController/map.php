<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 4/11/2018
 * Time: 10:31 AM
 */
require_once __DIR__ . "/../repositories/VehicleRepository.php";
require_once __DIR__ . "/../models/Vehicle.php";
require_once __DIR__ . "/../lib/SupplyDB.php";
$vehicles = VehicleRepository::getAllVehicles();


$access_token = 'token'; // access_token received from /oauth2/token call
$ch = curl_init('https://jyoung9.create.stedwards.edu/Zume/API/Vehicle.php'); // URL of the call
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER,
    array("Authorization: Bearer $access_token"));
// execute the api call
$result = curl_exec($ch);
// display the json response
print_r(json_decode($result, true));

function forceHTTPS()
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'])
    {
        return;
    }
    else
    {
        $requestURL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header("Location: https://$requestURL");
        exit();
    }
}


?>




<html lang="en">

<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>Zum: Rider View</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-ride/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-ride/css/modern-business.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../bootstrap-ride/vendor/jquery/jquery.min.js"></script>
    <script src="../bootstrap-ride/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdLbIKfOqZpLFI9-X76P81ADhY5nxFNps"></script>
    <script type ="text/javascript" src="v3_epoly.js"></script>
    <script type="text/javascript">

        var map;
        var directionDisplay;
        var directionsService;
        var stepDisplay;
        var markerArray = [];
        var position;
        var marker = null;
        var polyline = null;
        var poly2 = null;
        var speed = 0.000005, wait = 1;
        var infowindow = null;
        var vehicleMarkers = [];

        var myPano;
        var panoClient;
        var nextPanoId;
        var timerHandle = null;

        function createMarker(latlng, label, html) {
            // alert("createMarker("+latlng+","+label+","+html+","+color+")");
            var contentString = '<b>'+label+'</b><br>'+html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: label,
                zIndex: Math.round(latlng.lat()*-100000)<<5
            });
            marker.myname = label;
            // gmarkers.push(marker);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(contentString);
                infowindow.open(map,marker);
            });
            return marker;
        }

        // This function creates and displays google map
        // with vehicle locations displayed as icons
        function initialize() {
            infowindow = new google.maps.InfoWindow(
                {
                    size: new google.maps.Size(150,50)
                });


            // Instantiate a directions service.
            directionsService = new google.maps.DirectionsService();

            // Create a map and center it on Austin.
            var myOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);

            address = 'Austin'
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                map.setCenter(results[0].geometry.location);
            });


            // Create a renderer for directions and bind it to the map.
            var rendererOptions = {
                map: map
            }
            directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

            // Instantiate an info window to hold step text.
            stepDisplay = new google.maps.InfoWindow();

            polyline = new google.maps.Polyline({
                path: [],
                strokeColor: '#FF0000',
                strokeWeight: 3
            });
            poly2 = new google.maps.Polyline({
                path: [],
                strokeColor: '#FF0000',
                strokeWeight: 3
            });
            let iconBase = 'http://afragapa.create.stedwards.edu/zum/';
            let icons = {
                zumCar: {
                    icon: iconBase + 'zumCar.png'
                },
            };
            let vehicles = JSON.parse('[{"vehicleID":"1","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"1GCEK14K8RE106083","vehicleLPN":"000-000","vehicleStatus":"1","vehicleLat":"30.274666","vehicleLong":"-97.740349"},{"vehicleID":"2","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"5TFNX4CN0EX043350","vehicleLPN":"000-001","vehicleStatus":"1","vehicleLat":"30.266962","vehicleLong":"-97.772858"},{"vehicleID":"3","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"1FMRU17L41LA00287","vehicleLPN":"000-002","vehicleStatus":"1","vehicleLat":"30.232895","vehicleLong":"-97.747314"},{"vehicleID":"4","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"1GBJ6C1BX9F410906","vehicleLPN":"000-003","vehicleStatus":"1","vehicleLat":"30.524225","vehicleLong":"-97.858322"},{"vehicleID":"5","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"1C3EL55R16N160194","vehicleLPN":"000-004","vehicleStatus":"1","vehicleLat":"30.276098","vehicleLong":"-97.753288"},{"vehicleID":"6","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"3GYFNFEYXAS557256","vehicleLPN":"000-005","vehicleStatus":"1","vehicleLat":"30.255571","vehicleLong":"-97.784271"},{"vehicleID":"7","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"JTDKN3DUXE1743422","vehicleLPN":"000-0006","vehicleStatus":"1","vehicleLat":"30.277122","vehicleLong":"-97.749802"},{"vehicleID":"8","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"5GTDN136468131752","vehicleLPN":"000-007","vehicleStatus":"1","vehicleLat":"30.218420","vehicleLong":"-97.688065"},{"vehicleID":"9","fleetID":"Phantom Fleet Services LLC","vehicleVIN":"2LMHJ5FK9EBL53994","vehicleLPN":"ABC-0008","vehicleStatus":"1","vehicleLat":"31.194084","vehicleLong":"-97.671089"}]');
            let vehicleLocs = [];

            console.log(vehicles);

            // for each vehicle in database, create a marker with
            // latlng and set to map
            let index = 0;
            vehicles.forEach(function(vehicle) {
                let marker = new google.maps.Marker({
                    position: new google.maps.LatLng(vehicle.vehicleLat,vehicle.vehicleLong),
                    icon: icons['zumCar']['icon'],
                    map: map
                });
                vehicleMarkers.push(marker);
                vehicleLocs += vehicle.vehicleLat + "," + vehicle.vehicleLong;
            });
        }


        var steps = []


        // this function calculates and displays a route
        // between a 'start' and 'end' location and then
        // calls startAnimation() to animate an icon moving
        // along the route
        function calcRoute(){

            if (timerHandle) { clearTimeout(timerHandle); }
            if (marker) { marker.setMap(null);}
            polyline.setMap(null);
            poly2.setMap(null);
            directionsDisplay.setMap(null);
            polyline = new google.maps.Polyline({
                path: [],
                strokeColor: '#FF0000',
                strokeWeight: 3
            });
            poly2 = new google.maps.Polyline({
                path: [],
                strokeColor: '#FF0000',
                strokeWeight: 3
            });
            // Create a renderer for directions and bind it to the map.
            var rendererOptions = {
                map: map
            }
            directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);


            // get 'start' and 'end' locations from form
            var start = document.getElementById("start").value;
            var end = document.getElementById("end").value;
            var travelMode = google.maps.DirectionsTravelMode.DRIVING

            var request = {
                origin: {lat: 30.274666, lng: -97.740349},

                destination: end,
                waypoints: [
                    {
                        location: start,
                        stopover: true
                    }],
                travelMode: travelMode
            };

            // Route the directions and pass the response to a
            // function to create markers for each step.
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK){
                    directionsDisplay.setDirections(response);

                    var bounds = new google.maps.LatLngBounds();
                    var route = response.routes[0];
                    startLocation = new Object();
                    endLocation = new Object();

                    // For each route, display summary information.
                    var path = response.routes[0].overview_path;
                    var legs = response.routes[0].legs;
                    for (i=0;i<legs.length;i++) {
                        if (i == 0) {
                            startLocation.latlng = legs[i].start_location;
                            startLocation.address = legs[i].start_address;
                            // marker = google.maps.Marker({map:map,position: startLocation.latlng});
                            marker = createMarker(legs[i].start_location,"start",legs[i].start_address,"green");
                        }
                        endLocation.latlng = legs[i].end_location;
                        endLocation.address = legs[i].end_address;
                        var steps = legs[i].steps;
                        for (j=0;j<steps.length;j++) {
                            var nextSegment = steps[j].path;
                            for (k=0;k<nextSegment.length;k++) {
                                polyline.getPath().push(nextSegment[k]);
                                bounds.extend(nextSegment[k]);
                            }
                        }
                    }

                    polyline.setMap(map);
                    map.fitBounds(bounds);
                    // createMarker(endLocation.latlng,"end",endLocation.address,"red");
                    map.setZoom(18);
                    startAnimation();
                }
            });
        }


        //
        var step = 50; // 5; // metres
        var tick = 100; // milliseconds
        var eol;
        var k=0;
        var stepnum=0;
        var speed = "";
        var lastVertex = 1;


        //=============== animation functions ======================
        function updatePoly(d) {
            // Spawn a new polyline every 20 vertices, because updating a 100-vertex poly is too slow
            if (poly2.getPath().getLength() > 20) {
                poly2=new google.maps.Polyline([polyline.getPath().getAt(lastVertex-1)]);
                // map.addOverlay(poly2)
            }

            if (polyline.GetIndexAtDistance(d) < lastVertex+2) {
                if (poly2.getPath().getLength()>1) {
                    poly2.getPath().removeAt(poly2.getPath().getLength()-1)
                }
                poly2.getPath().insertAt(poly2.getPath().getLength(),polyline.GetPointAtDistance(d));
            } else {
                poly2.getPath().insertAt(poly2.getPath().getLength(),endLocation.latlng);
            }
        }


        function animate(d) {
            // alert("animate("+d+")");
            if (d>eol) {
                map.panTo(endLocation.latlng);
                marker.setPosition(endLocation.latlng);
                return;
            }
            var p = polyline.GetPointAtDistance(d);
            map.panTo(p);
            marker.setPosition(p);
            updatePoly(d);
            timerHandle = setTimeout("animate("+(d+step)+")", tick);
        }


        function startAnimation() {
            eol=polyline.Distance();
            map.setCenter(polyline.getPath().getAt(0));
            // map.addOverlay(new google.maps.Marker(polyline.getAt(0),G_START_ICON));
            // map.addOverlay(new GMarker(polyline.getVertex(polyline.getVertexCount()-1),G_END_ICON));
            // marker = new google.maps.Marker({location:polyline.getPath().getAt(0)} /* ,{icon:car} */);
            // map.addOverlay(marker);
            poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(0)], strokeColor:"#0000FF", strokeWeight:10});
            // map.addOverlay(poly2);
            setTimeout("animate(50)",2000);  // Allow time for the initial map display
        }
        //=============== ~animation funcitons =====================


        // Finds closes marker on map to given lat long
        function findClosestMarker( lat1, lon1 ) {
            var pi = Math.PI;
            var R = 6371; //equatorial radius
            var distances = [];
            var closest = -1;

            for( i = 0; i < vehicleMarkers.length; i++ ) {
                var lat2 = vehicleMarkers[i].position.lat();
                var lon2 = vehicleMarkers[i].position.lng();

                var chLat = lat2-lat1;
                var chLon = lon2-lon1;

                var dLat = chLat*(pi/180);
                var dLon = chLon*(pi/180);

                var rLat1 = lat1*(pi/180);
                var rLat2 = lat2*(pi/180);

                var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(rLat1) * Math.cos(rLat2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                var d = R * c;

                distances[i] = d;
                if ( closest == -1 || d < distances[closest] ) {
                    closest = i;
                }
            }

            // (debug) The closest marker is:
            //console.log(vehicleMarkers[closest]);
            return vehicleMarkers[closest];
        }




    </script>
</head>


<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-white border-bottom" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/">Zum: Rider View</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerRegister.php">Rider Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerLogin.php">Rider Log Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sRegister.php">Supplier Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sLogin.php">Supplier Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<body onload="initialize()">

<div id="map" ></div>


<!-- Page Content -->
<div class="container">

    <br><br>

</div>

<!-- Marketing Icons Section -->
<div class="container">
    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card h-100">
                <h4 class="card-header">Vehicle Information </h4>
                <div class="card-body">
                    <p class="card-text">Your vehicle information will populate here once a ZumCar is called.</p>
                    <a href="#" class="btn btn-primary">Confirm Ride</a>
                </div>



            </div>
        </div>

        <div class="col-lg-6 mb-4">
            <div class="card h-100">
                <h4 class="card-header">Ride Addresses</h4>
                <div class="card-body">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <div id="tools">
                            <input id="start" type="text" class="form-control mb-2" name="Start Address" placeholder="Start Address">
                            <input id="end" type="text" class="form-control mb-2" name="End Address" placeholder="End Address">
                            <input type= "submit" class ="btn btn-primary" type="Request Ride" onclick="calcRoute();"/>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<!-- /.row -->


<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Zum 2018</p>
    </div>
    <!-- /.container -->
</footer>

</body>



</html>