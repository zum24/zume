<?php
/**
 * Created by IntelliJ IDEA.
 * User: Johnny
 * Date: 4/11/2018
 * Time: 10:31 AM
 */
require_once __DIR__ . "/../repositories/VehicleRepository.php";
require_once __DIR__ . "/../models/Vehicle.php";
require_once __DIR__ . "/../lib/SupplyDB.php";
//$vehicles = VehicleRepository::getVehiclesByFleetID()
$vehicles = VehicleRepository::getAllVehicles();


$access_token = 'token'; // access_token received from /oauth2/token call
$ch = curl_init('https://jyoung9.create.stedwards.edu/Zume/API/Vehicle.php'); // URL of the call
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER,
    array("Authorization: Bearer $access_token"));
// execute the api call
$result = curl_exec($ch);
// display the json response
print_r(json_decode($result, true));

function forceHTTPS()
{
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'])
    {
        return;
    }
    else
    {
        $requestURL = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header("Location: https://$requestURL");
        exit();
    }
}


?>




<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zum: Rider View</title>

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap-ride/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../bootstrap-ride/css/modern-business.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../bootstrap-ride/vendor/jquery/jquery.min.js"></script>
    <script src="../bootstrap-ride/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdLbIKfOqZpLFI9-X76P81ADhY5nxFNps"></script>

    <script>

        function initMap() {

            let austin = new google.maps.LatLng(30.267153, -97.743061);
            let map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: austin
            });
            let iconBase = 'http://afragapa.create.stedwards.edu/zum/';
            let icons = {
                zumCar: {
                    icon: iconBase + 'zumCar.png'
                },
            };
            let vehicles = JSON.parse('<?php echo json_encode($vehicles) ?>');

            console.log(vehicles);
            vehicles.forEach(function(vehicle) {
                let marker = new google.maps.Marker({
                    position: new google.maps.LatLng(vehicle.vehicleLat,vehicle.vehicleLong),
                    icon: icons['zumCar']['icon'],
                    map: map
                });
            });
            infowindow = new google.maps.InfoWindow(
                {
                    size: new google.maps.Size(150,50)
                });

            // Create a map and center it on Austin.
            var myOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            address = 'Austin'
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                map.setCenter(results[0].geometry.location);
            });


        }


    </script>




</head>




<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-white border-bottom" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/">Zum: Supplier View</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerRegister.php">Rider Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/customerLogin.php">Rider Log Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sRegister.php">Supplier Register</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="https://aalcanta.create.stedwards.edu/zum/ViewController/sLogin.php">Supplier Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<body onload="initMap()">

<div id="map"></div>

<!-- Page Content -->
<div class="container">

    <br><br>

</div>

<!-- Marketing Icons Section -->
<div class ="container">
<div class="row">
    <div class="col-lg-5 mb-4">
        <div class="card h-100">
            <h4 class="card-header">Fleet Information </h4>
            <div class="card-body">
                <p class="card-text">
                <p id="vehicleDisplay"></p>
                    <script>
                        let vehicles = JSON.parse('<?php echo json_encode($vehicles) ?>');
                        document.getElementById("vehicleDisplay").innerHTML = vehicles[0].fleetID + "<br/>";
                        vehicles.forEach(function(vehicle) {
                            let status = "";
                            if (vehicle.vehicleStatus == true){
                                status = "Available";
                            }
                            else { status = "Unavailable";}
                            document.getElementById("vehicleDisplay").innerHTML += "Vehicle No\. " +
                                vehicle.vehicleID + " | Location: " + "(" + vehicle.vehicleLat + "," + vehicle.vehicleLong + ") | "
                                + status +  "<br/>";
                        });
                    </script>
                </p>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card h-100">
            <h4 class="card-header"> Today's Profits </h4>
            <div class="card-body">
                <p class="card-text">Today's profits will populate here.</p>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="card h-100">
            <h4 class="card-header">Vehicles in Use </h4>
            <div class="card-body">
                <p class="card-text">Vehicles that have riders in them will populate here.</p>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
</div>
</div>
<!-- /.row -->


<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Zum 2018</p>
    </div>
    <!-- /.container -->
</footer>



</body>

</html>
